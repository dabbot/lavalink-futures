//! Module containing structs for interacting with Lavalink nodes and playing
//! audio for guilds.

use crate::{
    error::{Error, Result},
    nodes::Node,
};
use futures::channel::mpsc::UnboundedSender;
use lavalink::model::{
    Band,
    Destroy,
    Equalizer,
    Pause,
    Play,
    Seek,
    Stop,
    Volume,
};
use parking_lot::{Mutex, RwLock};
use serde_json;
use std::{
    collections::HashMap,
    sync::Arc,
};
use tokio_tungstenite::tungstenite::Message;

/// A light wrapper around a hashmap keyed by guild IDs with audio players.
#[derive(Clone, Debug, Default)]
pub struct AudioPlayerManager {
    players: HashMap<u64, Arc<RwLock<AudioPlayer>>>,
}

impl AudioPlayerManager {
    /// Creates a new default `AudioPlayerManager`.
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates an audio player for the guild of the given ID.
    ///
    /// The `sender` must be a clone of [`Node::user_to_node`].
    ///
    /// It may be preferable to use [`NodeManager::create_player`].
    ///
    /// [`Node::user_to_node`]: ../nodes/struct.Node.html#structfield.user_to_node
    /// [`NodeManager::create_player`]: ../nodes/struct.NodeManager.html#method.create_player
    pub fn create(
        &mut self,
        node: Arc<Mutex<Node>>,
        guild_id: u64,
        sender: UnboundedSender<Result<Message>>,
    ) -> Result<Arc<RwLock<AudioPlayer>>> {
        if self.players.contains_key(&guild_id) {
            return Err(Error::PlayerAlreadyExists);
        }

        let player = Arc::new(RwLock::new(AudioPlayer::new(
            node,
            guild_id,
            sender,
        )));

        self.players.insert(guild_id, Arc::clone(&player));

        Ok(player)
    }

    /// Retrieves an immutable reference to the audio player for the guild, if
    /// it exists.
    pub fn get(&self, guild_id: &u64) -> Option<Arc<RwLock<AudioPlayer>>> {
        self.players.get(guild_id).cloned()
    }

    /// Retrieves a player by ID if it exists, or creates a new one.
    pub fn get_or_create(
        &mut self,
        node: Arc<Mutex<Node>>,
        guild_id: u64,
        sender: UnboundedSender<Result<Message>>,
    ) -> Arc<RwLock<AudioPlayer>> {
        if let Some(player) = self.get(&guild_id) {
            return player;
        }

        let player = Arc::new(RwLock::new(AudioPlayer::new(
            node,
            guild_id,
            sender,
        )));
        self.players.insert(guild_id, Arc::clone(&player));

        return player;
    }

    /// Whether the manager contains a player for the given guild.
    pub fn has(&self, guild_id: &u64) -> bool {
        self.players.contains_key(guild_id)
    }

    /// Returns an immutable reference to the map of players.
    #[inline]
    pub fn players(&self) -> &HashMap<u64, Arc<RwLock<AudioPlayer>>> {
        &self.players
    }

    /// Removes an audio player by guild ID.
    ///
    /// Calls [`AudioPlayer::leave`] if it is connected.
    ///
    /// [`AudioPlayer::leave`]: struct.AudioPlayer.html#method.leave
    pub fn remove(&mut self, guild_id: &u64) -> bool {
        self.players.remove(guild_id).is_some()
    }
}

/// A struct containing the state of a guild's audio player.
#[derive(Clone, Debug, Serialize)]
pub struct AudioPlayer {
    /// The ID of the guild that the player represents.
    pub guild_id: u64,
    /// The node that this player is linked to.
    #[serde(skip_serializing)]
    pub node: Arc<Mutex<Node>>,
    /// Whether the player is paused.
    pub paused: bool,
    /// The estimated position of the player.
    pub position: i64,
    #[serde(skip)]
    sender: UnboundedSender<Result<Message>>,
    /// The current time of the player.
    pub time: i64,
    /// The track that the player is playing.
    pub track: Option<String>,
    /// The volume setting, on a scale of 0 to 150.
    pub volume: i32,
}

impl AudioPlayer {
    /// Creates a new audio player.
    ///
    /// Using [`AudioPlayerManager::create`] or [`NodeManager::create_player`]
    /// is the preferred method of creating a new player.
    ///
    /// [`AudioPlayerManager::create`]: struct.AudioPlayerManager.html#method.create
    /// [`NodeManager::create_player`]: ../nodes/struct.NodeManager.html#method.create_player
    pub fn new(
        node: Arc<Mutex<Node>>,
        guild_id: u64,
        sender: UnboundedSender<Result<Message>>,
    ) -> Self {
        Self {
            paused: false,
            position: 0,
            time: 0,
            track: None,
            volume: 100,
            guild_id,
            node,
            sender,
        }
    }

    /// Sends a message to Lavalink telling it to destroy the player.
    pub fn destroy(&mut self) -> Result<()> {
        let msg = serde_json::to_string(&Destroy::new(self.guild_id.to_string()))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to update the equalizer.
    pub fn equalizer(&mut self, bands: Vec<Band>) -> Result<()> {
        let msg = serde_json::to_string(&Equalizer::new(
            self.guild_id.to_string().as_ref(),
            bands,
        ))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to either pause or unpause the
    /// player.
    pub fn pause(&mut self, pause: bool) -> Result<()> {
        let msg = serde_json::to_string(&Pause::new(
            &self.guild_id.to_string()[..],
            pause,
        ))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to play a track with optional
    /// configuration settings.
    pub fn play(
        &mut self,
        track: &str,
        start_time: Option<u64>,
        end_time: Option<u64>,
    ) -> Result<()> {
        let msg = serde_json::to_string(&Play::new(
            &self.guild_id.to_string()[..],
            track,
            start_time,
            end_time,
        ))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to seek the player to a certain
    /// position.
    pub fn seek(&mut self, position: i64) -> Result<()> {
        let msg = serde_json::to_string(&Seek::new(
            &self.guild_id.to_string()[..],
            position,
        ))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to stop the player.
    pub fn stop(&mut self) -> Result<()> {
        let msg = serde_json::to_string(&Stop::new(
            &self.guild_id.to_string()[..],
        ))?;

        self.send(msg)
    }

    /// Sends a message to Lavalink telling it to mutate the volume setting.
    pub fn volume(&mut self, volume: i32) -> Result<()> {
        let msg = serde_json::to_string(&Volume::new(
            &self.guild_id.to_string()[..],
            volume,
        ))?;

        self.send(msg)
    }

    /// Sends a WebSocket message over the node.
    ///
    /// You should prefer using one of the other methods where it makes sense.
    #[inline]
    pub fn send(&mut self, message: String) -> Result<()> {
        self.sender
            .start_send(Ok(Message::text(message)))
            .map(|_| ())
            .map_err(From::from)
    }

    /// Immutable reference to the sender to the node to relay messages to
    /// Lavalink.
    #[inline]
    pub fn sender(&self) -> &UnboundedSender<Result<Message>> {
        &self.sender
    }

    /// Mutable reference to the sender to the node to relay messages to
    /// Lavalink.
    #[inline]
    pub fn sender_mut(&mut self) -> &mut UnboundedSender<Result<Message>> {
        &mut self.sender
    }
}
