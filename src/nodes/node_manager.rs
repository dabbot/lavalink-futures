use crate::{
    error::{Error, Result},
    nodes::{Node, NodeConfig, NodeMessage},
    player::{AudioPlayer, AudioPlayerManager},
};
use futures::channel::mpsc::UnboundedReceiver;
use parking_lot::{Mutex, RwLock};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use std::i32;

/// A struct responsible for connecting to Lavalink nodes and providing
/// shortcuts for audio player usage.
pub struct NodeManager {
    /// HashMap of nodes, keyed by the host.
    pub nodes: Arc<Mutex<HashMap<String, Arc<Mutex<Node>>>>>,
    /// The player manager holding all of the audio players for nodes managed
    /// under the instance of a `NodeManager`.
    pub player_manager: Arc<Mutex<AudioPlayerManager>>,
    /// The current timeout to use for how long to wait for a message from
    /// Lavalink before reconnecting.
    ///
    /// A message may not be sent during the time period if the connection is
    /// sporadically interrupted or if Lavalink hangs the connection.
    pub timeout: Duration,
}

impl NodeManager {
    /// Creates a new NodeManager.
    ///
    /// Requires a handle to the tokio Core in use and an instance of a type
    /// implementing the [`EventHandler`] trait.
    ///
    /// [`EventHandler`]: ../trait.EventHandler.html
    pub fn new(timeout: Duration) -> Self {
        Self {
            nodes: Arc::new(Mutex::new(HashMap::new())),
            player_manager: Arc::new(Mutex::new(AudioPlayerManager::default())),
            timeout,
        }
    }

    /// Adds a new node to be managed.
    ///
    /// This will add the node to [`nodes`] once the connection successfully
    /// resolves.
    ///
    /// Resolves to an error if there was a problem connecting to the node.
    ///
    /// [`nodes`]: #structfield.nodes
    pub async fn add_node(
        &self,
        config: NodeConfig,
    ) -> Result<(Arc<Mutex<Node>>, UnboundedReceiver<NodeMessage>)> {
        trace!("Adding node with config: {:?}", config);

        let nodes = Arc::clone(&self.nodes);

        let host = config.host.clone();
        let (node, rx) = await!(Node::connect(
            config,
            Arc::clone(&self.player_manager),
            self.timeout,
        ))?;
        trace!("Added node: {:?}", node);

        let locked = Arc::new(Mutex::new(node));

        nodes.lock().insert(host, Arc::clone(&locked));

        Ok((locked, rx))
    }

    /// Determines the best node, if any.
    ///
    /// This does not return the node, but does return the host (keyed in
    /// [`nodes`]).
    ///
    /// [`nodes`]: #structfield.nodes
    pub fn best_node(&self) -> Option<String> {
        trace!("Determining best node");

        let mut record = i32::MAX;
        let mut best = None;

        let nodes = self.nodes.lock();

        for (name, node) in nodes.iter() {
            let total = node.lock().penalty().unwrap_or(0);

            debug!("Penalty of {}: {}; record: {}", name, total, record);

            if total < record {
                best = Some(name.clone());
                record = total;
            }
        }

        debug!("Best node: {:?}", best);

        best
    }

    /// Closes a node by host.
    ///
    /// Returns whether closing the node was successful. This can fail if the
    /// node is not recognized by host.
    ///
    /// Returns `Ok(true)` if the closing was successful. Returns `Ok(false)` if
    /// it wasn't.
    pub async fn close<'a>(&'a self, host: &'a str) -> Result<bool> {
        trace!("Closing node: {}", host);

        match self.nodes.lock().get_mut(host) {
            Some(host) => {
                trace!("Node exists");

                let mut host = host.lock();

                await!(host.close())?;

                trace!("Closed node");

                Ok(true)
            },
            None => {
                trace!("Node does not exist");

                Ok(false)
            },
        }
    }

    /// Closes all of the nodes owned by the manager.
    ///
    /// This is also automatically called when the instance is dropped.
    // **NOTE**: This can _NOT_ call `close`, as `nodes` is already mutably
    // borrowed here.
    pub async fn close_all(&self) -> Result<()> {
        trace!("Closing all nodes");

        let nodes = self.nodes.lock();

        for node in nodes.values() {
            let mut node = node.lock();

            await!(node.close())?;
        }

        Ok(())
    }

    /// Creates a new player using a [`Node`].
    ///
    /// [`Node`]: struct.Node.html
    pub fn create_player<'a>(
        &'a self,
        guild_id: u64,
        host: Option<&str>,
    ) -> Result<()> {
        let node = match host {
            Some(host) => self.nodes.lock().get(host).cloned().ok_or(Error::None)?,
            None => {
                self.best_node()
                    .and_then(|name| self.nodes.lock().get(&name).cloned())
                    .ok_or(Error::None)?
            },
        };

        let tx = node.lock().tx().clone();

        self.player_manager
            .lock()
            .create(node, guild_id, tx)
            .map(|_| ())
    }

    /// Retrieves a node by host.
    pub fn get_node(&self, host: &str) -> Option<Arc<Mutex<Node>>> {
        self.nodes.lock().get(host).cloned()
    }

    /// Retrieves a player by ID, creating it if it does not exist.
    pub fn player(
        &self,
        guild_id: u64,
        host: Option<&str>,
    ) -> Result<Arc<RwLock<AudioPlayer>>> {
        if let Some(player) = self.player_manager.lock().get(&guild_id) {
            debug!("Grabbing existing player");

            return Ok(player);
        }

        let node = match host {
            Some(host) => self.nodes.lock().get(host).cloned().ok_or(Error::None)?,
            None => {
                self.best_node()
                    .and_then(|name| self.nodes.lock().get(&name).cloned())
                    .ok_or(Error::None)?
            },
        };

        let tx = node.lock().tx().clone();

        Ok(self.player_manager.lock().get_or_create(node, guild_id, tx))
    }

    /// Removes a player by guild ID.
    ///
    /// Returns `Ok(true)` if the player existed and was removed. Returns
    /// `Ok(false)` if the player did not exist.
    pub fn remove_player(&self, guild_id: &u64) -> bool {
        self.player_manager.lock().remove(guild_id)
    }
}
