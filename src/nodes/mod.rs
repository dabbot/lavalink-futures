//! Structures for connecting to and interacting with Lavalink nodes.

mod node;
mod node_manager;

pub use self::node::{Node, NodeMessage};
pub use self::node_manager::NodeManager;

use lavalink::model::Stats;

/// Configuration identifying the bot user when connecting to a Lavalink node
/// via [`Node::connect`].
///
/// [`Node::connect`]: struct.Node.html#method.connect
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct NodeConfig {
    /// The address of the Lavalink node.
    ///
    /// For example, this may be `0.0.0.0:20000`.
    pub host: String,
    /// The ID of the bot user.
    pub user_id: String,
    /// The password used to connect to the Lavalink instance.
    pub password: String,
    /// The number of shards that the Discord client user (bot) is currently
    /// using. This must be the same number of shards as the bot in total.
    pub num_shards: u64,
}

/// State about a node.
#[derive(Clone, Debug, Default)]
pub struct State {
    /// Statistics about the node's load and players, if there is any.
    pub stats: Option<Stats>,
}
