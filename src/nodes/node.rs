use crate::{
    error::Result,
    nodes::{NodeConfig, State},
    player::*,
};
use futures::compat::{Future01CompatExt, Stream01CompatExt};
use futures::sink::SinkExt;
use futures::prelude::*;
use futures::channel::mpsc::{
    self,
    UnboundedReceiver,
    UnboundedSender,
};
use futures::select as async_select;
use lavalink::model::{Event, IncomingMessage, PlayerUpdate, Stats};
use parking_lot::Mutex;
use serde_json;
use std::borrow::Cow;
use std::marker::Unpin;
use std::process::Command;
use std::result::Result as StdResult;
use std::str::FromStr;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::prelude::{Future as Future01, Stream as Stream01};
use tokio::{
    reactor::Handle,
    timer::Delay,
};
use tokio_process::CommandExt;
use tokio_tungstenite::{
    self,
    tungstenite::{
        handshake::client::Request,
        Error as TungsteniteError,
        Message,
    },
};
use url::Url;

/// A message from Lavalink or a status update about the connection.
#[derive(Clone, Debug)]
pub enum NodeMessage {
    /// A message from Lavalink.
    Bytes(Vec<u8>),
}

/// The state of a connection to a Lavalink Node.
#[derive(Debug)]
pub struct Node {
    /// The address of the connected node.
    pub host: String,
    /// The password of the node.
    pub password: String,
    /// The state of the node, containing statistics like load averages.
    pub state: Arc<Mutex<State>>,
    tx: UnboundedSender<Result<Message>>,
}

impl Node {
    /// Connects to a Lavalink node.
    ///
    /// Requires a Handle to the tokio Core in use, configuration identifying
    /// the node, and an Rc to the audio player manager and handler
    /// implementation.
    ///
    /// It may be preferable to connect to a Node via [`NodeManager::add_node`].
    ///
    /// [`NodeManager::add_node`]: struct.NodeManager.html#method.add_node
    pub async fn connect(
        config: NodeConfig,
        player_manager: Arc<Mutex<AudioPlayerManager>>,
        timeout: Duration,
    ) -> Result<(Self, UnboundedReceiver<NodeMessage>)> {
        let url = Url::from_str(&format!("ws://{}", config.host))?;

        let mut headers = Vec::with_capacity(3);
        headers.push(("Authorization".to_owned(), config.password.clone()));
        headers.push(("Num-Shards".to_owned(), config.num_shards.to_string()));
        headers.push(("User-Id".to_owned(), config.user_id.clone()));

        let (node_to_user, user_from_node) = mpsc::unbounded();
        let (user_to_node, node_from_user) = mpsc::unbounded();

        let state = Arc::new(Mutex::new(State::default()));

        tokio::spawn(reconnect(
            headers,
            config.host.clone(),
            url,
            node_to_user,
            node_from_user,
            Arc::clone(&player_manager),
            Arc::clone(&state),
            timeout,
        ).map_err(|why| {
            warn!("Err with reconnect loop: {:?}", why);
        }).boxed().compat());

        Ok((Self {
            host: config.host,
            password: config.password,
            state,
            tx: user_to_node,
        }, user_from_node))
    }

    /// Sends a close code over the WebSocket, terminating the connection.
    ///
    /// **Note**: This does _not_ remove it from the manager operating the node.
    /// Prefer to close nodes via the manager.
    pub async fn close(&mut self) -> Result<()> {
        debug!("Closing node");

        await!(self.tx.close())?;

        Ok(())
    }

    /// Calculates the penalty of the node.
    ///
    /// Returns `None` if the internal [`state`] could not be accessed at the
    /// time or if there are not yet any stats. The state should never be
    /// inaccessible by only the library's usage, so you should be cautious
    /// about accessing it.
    pub fn penalty(&self) -> Option<i32> {
        debug!("Retrieving node penalty");

        let state = self.state.lock();
        let stats = state.stats.as_ref()?;

        let cpu = 1.05f64.powf(100f64 * stats.cpu.system_load) * 10f64 - 10f64;

        let (deficit_frame, null_frame) = match stats.frames.as_ref() {
            Some(frames) => {
                (
                    1.03f64.powf(500f64 * (f64::from(frames.average_deficit_per_minute) / 3000f64)) * 300f64 - 300f64,
                    (1.03f64.powf(500f64 * (f64::from(frames.average_nulled_per_minute) / 3000f64)) * 300f64 - 300f64) * 2f64,
                )
            },
            None => (0f64, 0f64),
        };

        Some(stats.playing_players + cpu as i32 + deficit_frame as i32 + null_frame as i32)
    }

    /// Sends a node over the WebSocket to the Lavalink node.
    ///
    /// ```rust,no_run
    /// #[macro_use]
    /// extern crate log;
    ///
    /// extern crate futures;
    /// extern crate lavalink_futures;
    /// extern crate serde_json;
    /// extern crate tokio;
    ///
    /// #
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<Error>> {
    /// #
    /// use futures::Future;
    /// use lavalink_futures::lavalink::model::Play;
    /// use lavalink_futures::nodes::{NodeConfig, NodeManager};
    ///
    /// let manager = NodeManager::new();
    /// let config = NodeConfig {
    ///     http_host: "127.0.0.1:14002".to_owned(),
    ///     websocket_host: "128.0.0.1:14001".to_owned(),
    ///     user_id: "249608697955745802".to_owned(),
    ///     password: "foo".to_owned(),
    ///     num_shards: 5,
    /// };
    ///
    /// let play = Play::new("381880193251409931", "blob here", None, None);
    /// let payload = serde_json::to_string(&play)?;
    ///
    /// let future = manager.add_node(config).map(|(node, rx)| {
    ///     if let Err(why) = node.lock().send(payload) {
    ///         warn!("Error sending play payload over WebSocket: {:?}", why);
    ///     }
    /// }).map(|_| ()).map_err(|_| ());
    ///
    /// tokio::run(future);
    /// #
    /// #     Ok(())
    /// # }
    /// ```
    pub fn send(
        &mut self,
        bytes: String,
    ) -> Result<()> {
        debug!("Sending message: {:?}", bytes);

        match self.tx.unbounded_send(Ok(Message::text(bytes))) {
            Ok(()) => {
                debug!("Sent message");

                Ok(())
            },
            Err(why) => {
                warn!("Err sending message: {:?}", why);

                unreachable!();
                // TODO
                // Err(From::from(why))
            },
        }
    }

    /// Immutable reference to the sender to the node to relay messages to
    /// Lavalink.
    #[inline]
    pub fn tx(&self) -> &UnboundedSender<Result<Message>> {
        &self.tx
    }
}

async fn from_lavalink(
    mut stream: impl Stream<Item = StdResult<Message, TungsteniteError>> + Unpin,
    to_user: UnboundedSender<NodeMessage>,
    player_manager: Arc<Mutex<AudioPlayerManager>>,
    ws_state: Arc<Mutex<State>>,
    timeout: Duration,
) -> Result<()> {
    loop {
        let mut retrieval = stream.next().fuse();
        let mut delay = Delay::new(Instant::now() + timeout).compat().fuse();

        let msg = async_select! {
            _ = delay => {
                warn!("Timed out waiting for a message");

                break;
            },
            res = retrieval => res,
        };

        match msg {
            Some(Ok(Message::Ping(data))) => {
                trace!("Received a ping: {:?}", data);

                continue;
            },
            Some(Ok(Message::Text(data))) => {
                trace!("Received text: {:?}", data);
                let bytes = data.into_bytes();

                handle_message(
                    &bytes,
                    Arc::clone(&ws_state),
                    Arc::clone(&player_manager),
                );

                let _ = to_user.unbounded_send(NodeMessage::Bytes(bytes));
            },
            Some(Ok(Message::Binary(data))) => {
                trace!("Received binary: {:?}", data);

                handle_message(
                    &data,
                    Arc::clone(&ws_state),
                    Arc::clone(&player_manager),
                );

                let _ = to_user.unbounded_send(NodeMessage::Bytes(data));
            },
            Some(Ok(Message::Pong(data))) => {
                warn!("Received a pong somehow? {:?}", data);
            },
            Some(Err(why)) => {
                warn!("Received msg is an err: {:?}", why);

                break;
            },
            None => {
                warn!("Received no message before timeout");

                break;
            },
        }
    }

    warn!("FROM_LAVALINK ended");

    Ok(())
}

async fn to_lavalink(
    node_from_user: &mut UnboundedReceiver<Result<Message>>,
    to_dump: UnboundedSender<Result<Message>>,
) -> Result<()> {
    while let Some(Ok(msg)) = await!(node_from_user.next()) {
        let msg = match msg {
            Message::Binary(bytes) => {
                let text = match String::from_utf8(bytes) {
                    Ok(utf8) => utf8,
                    Err(why) => {
                        warn!("Received invalid UTF8: {:?}", why);

                        continue;
                    },
                };

                Message::Text(text)
            },
            other => other,
        };

        if let Err(why) = to_dump.unbounded_send(Ok(msg)) {
            warn!("Err sending message to lavalink forwarder: {:?}", why);
        }
    }

    warn!("TO_LAVALINK ended");

    to_dump.close_channel();

    Ok(())
}

async fn reconnect(
    headers: Vec<(String, String)>,
    host: String,
    url: Url,
    node_to_user: UnboundedSender<NodeMessage>,
    mut node_from_user: UnboundedReceiver<Result<Message>>,
    player_manager: Arc<Mutex<AudioPlayerManager>>,
    state: Arc<Mutex<State>>,
    timeout: Duration,
) -> Result<()> {
    use futures::select;

    let headers = headers.into_iter()
        .map(|(k, v)| (Cow::from(k), Cow::from(v)))
        .collect::<Vec<_>>();

    loop {
        let request = Request {
            extra_headers: Some(headers.clone()),
            url: url.clone(),
        };

        let res: Result<()> = try {
            let (duplex, response) = await!(tokio_tungstenite::connect_async(
                request,
            ).compat())?;
            debug!("Response: {:?}", response);

            trace!("Node WS client connected");

            let (sink, stream) = duplex.split();
            let ws_state = Arc::clone(&state);

            let node_to_user2 = node_to_user.clone();

            let to_dump = {
                let (tx, rx) = mpsc::unbounded();

                let future = rx.compat().forward(sink).map(move |_| {
                    warn!("Connection to Lavalink closed on success");
                })
                .map_err(move |why| {
                    warn!("Connection to Lavalink closed on err: {:?}", why);
                });

                tokio::spawn(future);

                tx
            };

            let mut fut1 = from_lavalink(
                stream.compat(),
                node_to_user2.clone(),
                Arc::clone(&player_manager),
                Arc::clone(&ws_state),
                timeout,
            ).map_ok(|_| ()).map_err(|why| {
                warn!("Err with FROM_LAVALINK: {:?}", why);
            }).boxed().fuse();
            let mut fut2 = to_lavalink(
                &mut node_from_user,
                to_dump,
            ).map_err(|why| {
                warn!("Err with TO_LAVALINK: {:?}", why);
            }).boxed().fuse();

            let res = select! {
                res = fut1 => res,
                res = fut2 => res,
            };

            if let Err(why) = res {
                warn!("Err with lavalink connection: {:?}", why);
            }
        };

        if let Err(why) = res {
            warn!("Err with reconnect loop: {:?}", why);
        }

        let ssh_restart = Command::new("ssh")
            .arg("-i")
            .arg("./lhs_key")
            .arg(format!("root@{}", host.split(':').collect::<Vec<_>>().remove(0)))
            .arg("systemctl")
            .arg("restart")
            .arg("lavalink")
            .output_async_with_handle(&Handle::default());

        match await!(ssh_restart.compat()) {
            Ok(output) => {
                if !output.status.success() {
                    warn!("Non-success SSH: {:?}", output);
                }
            },
            Err(why) => {
                warn!("Err running SSH: {:?}", why);
            },
        }

        // Give Lavalink a few seconds to reboot.
        await!(Delay::new(Instant::now() + Duration::from_secs(10)).compat())?;
    }
}

fn handle_message(
    // todo: why is this not needed?
    bytes: &[u8],
    state: Arc<Mutex<State>>,
    player_manager: Arc<Mutex<AudioPlayerManager>>,
) {
    match serde_json::from_slice(bytes) {
        Ok(IncomingMessage::Event(event)) => {
            handle_event(&event, &player_manager);
        },
        Ok(IncomingMessage::PlayerUpdate(update)) => {
            handle_player_update(&update, &player_manager);
        },
        Ok(IncomingMessage::Stats(stats)) => {
            handle_state(stats, state);
        }
        Err(why) => {
            warn!("Error parsing received JSON: {:?}", why);
        },
    }
}

fn handle_event(event: &Event, player_manager: &Arc<Mutex<AudioPlayerManager>>) {
    let guild_id = event.guild_id();
    let id = match guild_id.parse() {
        Ok(id) => id,
        Err(_) => return,
    };

    let player_manager = player_manager.lock();

    let player = match player_manager.get(&id) {
        Some(player) => player,
        None => {
            warn!(
                "got invalid audio player update for guild {:?}",
                guild_id,
            );

            return;
        },
    };

    if let Event::TrackEnd(_) = event {
        // Set the player's track so nothing is playing, reset
        // the time, and reset the position
        let mut player = player.write();
        player.track = None;
        player.time = 0;
        player.position = 0;
    }
}

fn handle_player_update(
    update: &PlayerUpdate,
    player_manager: &Arc<Mutex<AudioPlayerManager>>,
) {
    let id = match update.guild_id.parse() {
        Ok(id) => id,
        Err(why) => {
            warn!("Err parsing guild ID in {:?}: {:?}", update, why);

            return;
        },
    };

    match player_manager.lock().get(&id) {
        Some(player) => {
            let mut player = player.write();
            player.time = update.state.time as i64;

            if let Some(position) = update.state.position {
                player.position = position;
            }
        },
        None => {
            warn!("Invalid player update received for guild {}", id);
        },
    }
}

// todo: should this be needed?
fn handle_state(stats: Stats, state: Arc<Mutex<State>>) {
    state.lock().stats = Some(stats);
}
