//! [![ci-badge][]][ci] [![license-badge][]][license] [![docs-badge][]][docs] [![rust badge]][rust link]
//!
//! # lavalink-futures
//!
//! `lavalink-futures` is an asynchronous, Futures-based client implementation for
//! [Lavalink], built on top of [lavalink.rs].
//!
//! ### Installation
//!
//! Add the following to your `Cargo.toml`:
//!
//! ```toml
//! [dependencies]
//! lavalink-futures = { git = "https://github.com/dabbotorg/lavalink-futures" }
//! ```
//!
//! And then at the top of your `main.rs`/`lib.rs`:
//!
//! ```rust,no_run
//! extern crate lavalink_futures;
//! ```
//!
//! ### Examples
//!
//! Creating a `NodeManager`, connecting to a `Node` with it, and then playing a
//! track for a guild:
//!
//! ```rust,no_run
//! extern crate futures;
//! extern crate lavalink_futures;
//! extern crate tokio;
//!
//! # use std::error::Error;
//! #
//! # fn try_main() -> Result<(), Box<Error>> {
//! #
//! use futures::Future;
//! use lavalink_futures::nodes::{NodeConfig, NodeManager};
//! use std::env;
//!
//! let mut manager = NodeManager::new();
//!
//! let done = manager.add_node(NodeConfig {
//!     http_host: env::var("LAVALINK_HTTP_HOST")?,
//!     num_shards: env::var("DISCORD_SHARD_COUNT")?.parse()?,
//!     password: env::var("LAVALINK_PASSWORD")?,
//!     user_id: env::var("DISCORD_USER_ID")?.parse()?,
//!     websocket_host: env::var("LAVALINK_WS_HOST")?,
//! }).map(|manager| {
//!     // Do more with the manager here, such as connecting to more nodes, creating
//!     // audio players, and/or attaching the node manager to some structure's
//!     // state.
//! }).map(|_| ()).map_err(|_| ());
//!
//! tokio::run(done);
//! #     Ok(())
//! # }
//! #
//! # fn main() {
//! #     try_main().unwrap();
//! # }
//! ```
//!
//! [Lavalink]: https://github.com/Frederikam/Lavalink
//! [lavalink.rs]: https://github.com/serenity-rs/lavalink.rs
//! [ci]: https://travis-ci.org/dabbotorg/lavalink-futures
//! [ci-badge]: https://img.shields.io/travis/dabbotorg/lavalink-futures.svg?style=flat-square
//! [docs]: https://docs.rs/crate/lavalink-futures
//! [docs-badge]: https://img.shields.io/badge/docs-online-2020ff.svg?style=flat-square
//! [LICENSE.md]: https://github.com/dabbotorg/lavalink-futures/blob/master/LICENSE.md
//! [license]: https://opensource.org/licenses/ISC
//! [license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
//! [rust badge]: https://img.shields.io/badge/rust-1.26+-93450a.svg?style=flat-square
//! [rust link]: https://blog.rust-lang.org/2018/05/10/Rust-1.26.html

#![deny(missing_docs)]
#![feature(async_await, await_macro, futures_api, try_blocks)]

#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;

pub extern crate lavalink;

pub mod nodes;
pub mod player;
pub mod reexports;

mod error;

pub use self::error::{Error, Result};
